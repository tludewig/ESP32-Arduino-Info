# ESP32-Arduino-Info

Prints out chip, sdk and wifi information using the *Arduino* framework.

## Compile requirements
- rename or copy private/build_flags.sample to private/build_flags.sh
  and change &lt;YOUR_WIFI_SSID> and &lt;YOUR_WIFI_PASSWORD>
